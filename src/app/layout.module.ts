import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { NavbarUsuarioNormalComponent } from './shared/components/navbar-usuario-normal/navbar-usuario-normal.component';
import { InicioComponent } from './modules/inicio/inicio.component';
import { CitasComponent } from './estetica/citas/citas.component';



@NgModule({
  imports: [
    CommonModule,
    RouterModule,
  ],
  declarations: [NavbarUsuarioNormalComponent,InicioComponent,CitasComponent],
  exports : [NavbarUsuarioNormalComponent]
})
export class LayoutModule { }