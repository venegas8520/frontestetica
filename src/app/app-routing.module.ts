import { FormularioServicioComponent } from './modules/formulario-servicio/formulario-servicio.component';
import { FormularioIniciarSesionComponent } from './shared/components/formulario-iniciar-sesion/formulario-iniciar-sesion.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CitasComponent } from './estetica/citas/citas.component';
import { AgendarCitaComponent } from './modules/agendar-cita/agendar-cita.component';
import { CatalogoProductosComponent } from './modules/catalogo-productos/catalogo-productos.component';
import { CatalogoServiciosComponent } from './modules/catalogo-servicios/catalogo-servicios.component';
import { InicioComponent } from './modules/inicio/inicio.component';
import { CardDetalleProductoComponent } from './shared/components/card-detalle-producto/card-detalle-producto.component';
import { CardDetalleServiciosComponent } from './shared/components/card-detalle-servicios/card-detalle-servicios.component';
import { CardProductoComponent } from './shared/components/card-producto/card-producto.component';
import { CardServiciosComponent } from './shared/components/card-servicios/card-servicios.component';
import { ModalAddUsuarioComponent } from './shared/components/modal-add-usuario/modal-add-usuario.component';
import { ModalSeleccionarPagoComponent } from './shared/components/modal-seleccionar-pago/modal-seleccionar-pago.component';
import { NavbarUsuarioNormalComponent } from './shared/components/navbar-usuario-normal/navbar-usuario-normal.component';
import { RegistroUsuarioComponent } from './shared/components/registro-usuario/registro-usuario.component';
import { ContactoComponent } from './modules/contacto/contacto.component';
import { EstilistaInfoComponent } from './modules/estilista-info/estilista-info.component';
import { EstilistasComponent } from './modules/estilistas/estilistas.component';
import { FormularioComponent } from './modules/formulario/formulario.component';
import { VerServiciosComponent } from './modules/ver-servicios/ver-servicios.component';
import { DashboardAdminComponent } from './estetica/dashboard-admin/dashboard-admin.component';
import { CitasPendientesComponent } from './estetica/citas-pendientes/citas-pendientes.component';
import { ProductosComponent } from './estetica/productos/productos.component';
import { UsuariosComponent } from './estetica/usuarios/usuarios.component';
import { PerfilComponent } from './usuarios/perfil/perfil.component';
import { EstilistasAdminComponent } from './estetica/estilistas-admin/estilistas-admin.component';
import { FormsServicioComponent } from './estetica/forms-servicio/forms-servicio.component';




const routes: Routes = [
  {
    path: 'citas',
    component: CitasComponent,
  }/* ,
  {
    path: 'citas-pendientes',
    component: CitasPendientesComponent
  } */,
  /* Navegacion a agendar cita */
  {
    path: 'agendar-cita',
    component: AgendarCitaComponent,
  },
  
  {
    path: 'inicio',
    component: InicioComponent,
  },
  {
    path: 'catalogo-servicios',
    component: CatalogoServiciosComponent,
  },
  {
    path: 'catalogo-productos',
    component: CatalogoProductosComponent,
  },
  {
    path: 'formulario-iniciar-sesion',
    component: FormularioIniciarSesionComponent,
  },
  {
    path: 'card-detalle-producto/:id',
    component: CardDetalleProductoComponent,
  },
  {
    path: 'card-detalle-servicio/:id',
    component: CardDetalleServiciosComponent,
  },
  {
    path: 'card-producto',
    component: CardProductoComponent,
  },
  {
    path: 'card-servicio',
    component: CardServiciosComponent,
  },
  {
    path: 'modal-add-usuario',
    component: ModalAddUsuarioComponent,
  },
  {
    path: 'modal-seleccionar-pago',
    component: ModalSeleccionarPagoComponent,
  },
  {
    path: 'navbar-usuario-normal',
    component:NavbarUsuarioNormalComponent,
  },
  {
    path: 'registro-usuario',
    component:RegistroUsuarioComponent,
  },
  {
    path: 'estilistas',
    component: EstilistasComponent,
  },
  {
    path: 'estilista-Info/:id',
    component: EstilistaInfoComponent,
  },
  {
    path: 'contacto',
    component: ContactoComponent,
  },
  {
    path: 'formulario',
    component: FormularioComponent,
  },
  {
    path: 'update/:id',
    component: FormularioComponent,
  },
  {
    path: 'formularioSS',
    component: FormularioServicioComponent,
  },
  {
    path: 'verService',
    component: VerServiciosComponent,
  },
  {
    path: 'dashboard-admin',
    component: DashboardAdminComponent,
  },
  {
    path: 'citas-pendientes',
    component: CitasPendientesComponent
  },
  {
    path: 'estilistas-admin',
    component: EstilistasAdminComponent

  },
  {
    path: 'productos-admin',
    component: ProductosComponent
  },
  {
    path: 'usuarios-admin',
    component: UsuariosComponent
  },
  ,
  {
    path: 'form-servicio',
    component: FormsServicioComponent
  },
  {
    path: 'update-servicio/:id',
    component: FormsServicioComponent
  },
  /* RUTAS PARA EL PERFIL DE USUARIO REGISTRADO */
  {
    path: 'perfil-registrado',
    component: PerfilComponent
  },
  {
    path: 'perfil-registrado/:id',
    component: PerfilComponent
  },


  

  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'inicio',
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
