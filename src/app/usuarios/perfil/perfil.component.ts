import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { SeriviciosService } from 'src/app/services/serivicios.service';

@Component({
  selector: 'app-perfil',
  templateUrl: './perfil.component.html',
  styleUrls: ['./perfil.component.css']
})
export class PerfilComponent implements OnInit {

  usuario = {
    _id: "",
    nombre: "",
    apellido: "",
    correo: "",
    password: ""
  }

  citas: any;
  constructor(private ES: SeriviciosService, private router: Router,private AR: ActivatedRoute) { }
  id: any;

  ngOnInit(): void {
    this.id= this.AR.snapshot.params['id'];
    this.obtenercitas();
    this.obtenerUser();
  }

  obtenercitas(){
    this.ES.getCitas().subscribe((data: any)=>{
      console.log(data);
      this.citas = data;
    });
  }

  deletes(id: any) {
    console.log(id);
    this.ES.deleteCita(id).
    subscribe( newp => {
      console.log(newp);
    
        this.router.navigate(['/perfil-registrado']);
     
    });
  }

  obtenerUser(){
    this.ES.getUser(this.id).subscribe((data: any)=>{
      console.log(data);
      this.usuario = data;
    });
  }

}
