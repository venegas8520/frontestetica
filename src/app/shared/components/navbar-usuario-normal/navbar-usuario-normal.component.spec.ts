import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NavbarUsuarioNormalComponent } from './navbar-usuario-normal.component';

describe('NavbarUsuarioNormalComponent', () => {
  let component: NavbarUsuarioNormalComponent;
  let fixture: ComponentFixture<NavbarUsuarioNormalComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NavbarUsuarioNormalComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NavbarUsuarioNormalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
