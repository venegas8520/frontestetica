import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ModalSeleccionarPagoComponent } from './modal-seleccionar-pago.component';

describe('ModalSeleccionarPagoComponent', () => {
  let component: ModalSeleccionarPagoComponent;
  let fixture: ComponentFixture<ModalSeleccionarPagoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ModalSeleccionarPagoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalSeleccionarPagoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
