import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CardDetalleServiciosComponent } from './card-detalle-servicios.component';

describe('CardDetalleServiciosComponent', () => {
  let component: CardDetalleServiciosComponent;
  let fixture: ComponentFixture<CardDetalleServiciosComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CardDetalleServiciosComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CardDetalleServiciosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
