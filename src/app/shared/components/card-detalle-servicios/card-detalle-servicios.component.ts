import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { SeriviciosService } from 'src/app/services/serivicios.service';

@Component({
  selector: 'app-card-detalle-servicios',
  templateUrl: './card-detalle-servicios.component.html',
  styleUrls: ['./card-detalle-servicios.component.css']
})
export class CardDetalleServiciosComponent implements OnInit {
  id: any;
  servicio = {
    _id: "",
    nombre: "",
    descripcion: "",
    costo: "",
    tipo: ""
  }

  constructor(private ES: SeriviciosService, private AR: ActivatedRoute) { }

  ngOnInit(): void {
    this.id= this.AR.snapshot.params['id'];
    this.obtenerServicio();
  }

  obtenerServicio(){
    this.ES.getServicio(this.id).subscribe((data: any)=>{
      console.log(data);
      this.servicio = data;
    });
  }

}
