import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { SeriviciosService } from 'src/app/services/serivicios.service';

@Component({
  selector: 'app-card-detalle-producto',
  templateUrl: './card-detalle-producto.component.html',
  styleUrls: ['./card-detalle-producto.component.css']
})
export class CardDetalleProductoComponent implements OnInit {
id: any;

producto = {
  _id: "",
  nombre: "",
  descripcion: "",
  precio: "",
  stock: "",
  foto: ""
}

  constructor(private ES: SeriviciosService, private AR: ActivatedRoute) { }

  ngOnInit(): void {
    this.id= this.AR.snapshot.params['id'];
    this.obtenerProducto();
  }

  obtenerProducto(){
    this.ES.getProducto(this.id).subscribe((data: any)=>{
      console.log(data);
      this.producto = data;
    });
  }
}
