import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CardDetalleProductoComponent } from './card-detalle-producto.component';

describe('CardDetalleProductoComponent', () => {
  let component: CardDetalleProductoComponent;
  let fixture: ComponentFixture<CardDetalleProductoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CardDetalleProductoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CardDetalleProductoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
