import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormularioIniciarSesionComponent } from './formulario-iniciar-sesion.component';

describe('FormularioIniciarSesionComponent', () => {
  let component: FormularioIniciarSesionComponent;
  let fixture: ComponentFixture<FormularioIniciarSesionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FormularioIniciarSesionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FormularioIniciarSesionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
