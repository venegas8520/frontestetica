import { Component, OnInit } from '@angular/core';
import { SeriviciosService } from '../../../services/serivicios.service';
import { Router } from '@angular/router';
import { FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-formulario-iniciar-sesion',
  templateUrl: './formulario-iniciar-sesion.component.html',
  styleUrls: ['./formulario-iniciar-sesion.component.css']
})
export class FormularioIniciarSesionComponent implements OnInit {
  loginForm;
  user;

 constructor(private ES: SeriviciosService, private router: Router, private formBuild : FormBuilder) {
  this.loginForm = formBuild.group({
    correo: ['', [Validators.required]],
    password: ['', [Validators.required]],
  });

  }

  ngOnInit(): void {
  }

  submitLog(){
    this.ES.logearUser(this.loginForm.value).subscribe((data: any)=>{
      if(data){
      console.log(data);
      this.user = data;
      if(this.user._id){
        this.router.navigate(['/perfil-registrado',this.user._id]);
      }else{
        this.router.navigate(['/inicio']);
      }
      } 
    });
  }

}





