import { ComponentFixture, TestBed } from '@angular/core/testing';

import { CardEstilistaComponent } from './card-estilista.component';

describe('CardEstilistaComponent', () => {
  let component: CardEstilistaComponent;
  let fixture: ComponentFixture<CardEstilistaComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ CardEstilistaComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(CardEstilistaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
