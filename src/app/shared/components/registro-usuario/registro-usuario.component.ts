import { Component, OnInit } from '@angular/core';
import { SeriviciosService } from '../../../services/serivicios.service';
import { Router } from '@angular/router';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';

@Component({
  selector: 'app-registro-usuario',
  templateUrl: './registro-usuario.component.html',
  styleUrls: ['./registro-usuario.component.css']
})
export class RegistroUsuarioComponent implements OnInit {
  userForm;
  usuarios: any;

  constructor(private formBuild: FormBuilder, private ES: SeriviciosService, private router: Router) {
    
    this.userForm = formBuild.group({
      nombre: ['', [Validators.required]],
      apellido:['', [ Validators.required]],
      correo:['', [ Validators.required]],
      password:['', [ Validators.required]],
     
    });

   }
  

  ngOnInit(): void {
   
  }

  

   PostUsuario(){
    console.log(this.userForm.value)
    this.ES.PostUser(this.userForm.value).subscribe((data: any) =>{
      console.log(data);
      this.router.navigate(['/inicio'])
    })
  } 

/* --
  detalles(id:number){
    console.log(id);
    this.router.navigate(['/estilista-Info',id]);
  } */

 /*  editar(id:number){
    console.log(id);
    this.router.navigate(['/update',id]);
  } */

  /* eliminar(id:number){
    console.log(id);
    this.ES.deleteEstilista(id).subscribe((data: any)=>{
      console.log(data);
      this.obtenerUser();
    }) */
    
  }



