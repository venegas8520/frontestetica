import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
HttpClient
HttpClient
@Injectable({
  providedIn: 'root'
})
export class SeriviciosService {

  url = "http://localhost:4000/api";

  constructor(private http: HttpClient) { }

// endpoints estilistas 
  getEstilistas(){
    return this.http.get(`${this.url}/estilistas`);
  }

  getEstilista(id: number){
    return this.http.get(`${this.url}/estilistas/${id}`);
  }

  insertEstilistas(estilista:any){
    return this.http.post(`${this.url}/estilistas`, estilista);
  }

  updateEstilista(form: any, id : any){
    return this.http.put(`${this.url}/estilistas/${id}`,form);
  }

  deleteEstilista(id: any){
    return this.http.delete(`${this.url}/estilistas/${id}`);
  }

// servicio cita

  agendarCita(cita: any){
    return this.http.post(`${this.url}/citas`, cita);
  }

  getCitas(){
    return this.http.get(`${this.url}/citas`);
  }

  getCita(id: number){
    return this.http.get(`${this.url}/citas/${id}`);
  }

  updateCita(form: any, id : any){
    return this.http.put(`${this.url}/citas/${id}`,form);
  }

  deleteCita(id: any){
    return this.http.delete(`${this.url}/citas/${id}`);
  }

  //endpoints servicios

  getServicios(){
    return this.http.get(`${this.url}/servicios`);
  }

  getServicio(id: number){
    return this.http.get(`${this.url}/servicios/${id}`);
  }

  insertServicio(servicio:any){
    return this.http.post(`${this.url}/servicios`, servicio);
  }

  updateServicio(form: any, id : any){
    return this.http.put(`${this.url}/servicios/${id}`,form);
  }

  deleteServicio(id: any){
    return this.http.delete(`${this.url}/servicios/${id}`);
  }


  //endoints productos
  getProductos(){
    return this.http.get(`${this.url}/productos`);
  }

  getProducto(id: number){
    return this.http.get(`${this.url}/productos/${id}`);
  }

  //endpoints Usuarios
  getUsers(){
    return this.http.get(`${this.url}/usuarios`);
  }
  getUser(id: number){
    return this.http.get(`${this.url}/usuarios/${id}`);
  }
  PostUser(usuario:any){
    return this.http.post(`${this.url}/usuarios`, usuario);
  }

  //endpoint login
  logearUser(usuario:any){
    return this.http.post(`${this.url}/usuarios/logeo`, usuario);
  }

}
