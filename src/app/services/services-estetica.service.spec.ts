import { TestBed } from '@angular/core/testing';

import { ServicesEsteticaService } from './services-estetica.service';

describe('ServicesEsteticaService', () => {
  let service: ServicesEsteticaService;

  beforeEach(() => {
    TestBed.configureTestingModule({});
    service = TestBed.inject(ServicesEsteticaService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
