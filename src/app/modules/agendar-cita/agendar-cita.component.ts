import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup, FormsModule, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { SeriviciosService } from 'src/app/services/serivicios.service';
FormGroup

@Component({
  selector: 'app-agendar-cita',
  templateUrl: './agendar-cita.component.html',
  styleUrls: ['./agendar-cita.component.css']
})
export class AgendarCitaComponent implements OnInit {
  citaForm;
  servicios: any;

  constructor(private formBuild : FormBuilder, private citaSer: SeriviciosService, private router: Router) {
    this.citaForm = formBuild.group({
      fecha: ['', [Validators.required]],
      hora:['', [ Validators.required]],
      nombre:['', [ Validators.required]],
      apellidos:['', [ Validators.required]],
      telefono:['', [ Validators.required]],
      servicio:['', [ Validators.required]],
      estado:['false', [ Validators.required]],
    });
   }

  ngOnInit(): void {
    this.obtenerServicios();
  }

  submitCita(){
    console.log(this.citaForm.value)
    this.citaSer.agendarCita(this.citaForm.value).subscribe((data: any) =>{
      console.log(data);
      // this.router.navigate(['/inicio'])
    })
  }

  obtenerServicios(){
    this.citaSer.getServicios().subscribe((data: any)=>{
      console.log(data);
      this.servicios = data;
    });
  }
}
