import { NavbarUsuarioNormalComponent } from './../../shared/components/navbar-usuario-normal/navbar-usuario-normal.component';
import { Component, OnInit } from '@angular/core';
import { NgModule } from '@angular/core';
import { CardProductoComponent } from 'src/app/shared/components/card-producto/card-producto.component';
import { CardServiciosComponent } from 'src/app/shared/components/card-servicios/card-servicios.component';
import { SeriviciosService } from 'src/app/services/serivicios.service';
import { Router } from '@angular/router';
CardProductoComponent
CardServiciosComponent
@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.component.html',
  styleUrls: ['./inicio.component.css'],

})
export class InicioComponent implements OnInit {

  productos: any;
  estilistas: any;

  constructor(private ES: SeriviciosService, private router: Router) {}

  ngOnInit(): void {
    this.obtenerProductos();
    this.obtenerEstilistas();
  }
  obtenerEstilistas(){
    this.ES.getEstilistas().subscribe((data: any)=>{
      console.log(data);
      this.estilistas = data;
    });
  }
  obtenerProductos(){
    this.ES.getProductos().subscribe((data: any)=>{
      console.log(data);
      this.productos = data;
    });
  }

  detalles(id:number){
    console.log(id);
    this.router.navigate(['/card-detalle-producto',id]);
  }
}
