import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { SeriviciosService } from 'src/app/services/serivicios.service';
SeriviciosService
ActivatedRoute
@Component({
  selector: 'app-formulario',
  templateUrl: './formulario.component.html',
  styleUrls: ['./formulario.component.css']
})
export class FormularioComponent implements OnInit {

  estilista = {
    id: "",
    nombre: "",
    apellido: "",
    edad: "",
    especialidad: "",
    direccion: "",
    telefono: "",
    correo: "",
    foto: ""
  }

  constructor(private ES: SeriviciosService, private router: Router, private AR: ActivatedRoute) { }
  id:any;
  ngOnInit(): void {
    this.id= this.AR.snapshot.params['id'];
    if(this.id){
      this.ES.getEstilista(this.id).subscribe((data: any)=>{
        console.log(data);
        this.estilista = data;
      });
    }
  }

  save(form: NgForm){
    if(this.id){
      console.log(form.value);
      this.ES.updateEstilista(form.value, this.id).subscribe((data:any)=>{
        console.log(data);
        this.router.navigate(['estilistas-admin']);
      });
    }else{
      console.log(form.value);
    this.ES.insertEstilistas(form.value).subscribe((data: any) => {
      if(data){
        console.log(data);
        this.router.navigate(['/estilistas-admin']);
      }
    });
    }
  }

}
