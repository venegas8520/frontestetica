import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SeriviciosService } from 'src/app/services/serivicios.service';

@Component({
  selector: 'app-catalogo-productos',
  templateUrl: './catalogo-productos.component.html',
  styleUrls: ['./catalogo-productos.component.css'],
})
export class CatalogoProductosComponent implements OnInit {
  productos: any;
  constructor(private ES: SeriviciosService, private router: Router) {}

  ngOnInit(): void {
    this.obtenerProductos();
  }

  obtenerProductos(){
    this.ES.getProductos().subscribe((data: any)=>{
      console.log(data);
      this.productos = data;
    });
  }

  detalles(id:number){
    console.log(id);
    this.router.navigate(['/card-detalle-producto',id]);
  }
}