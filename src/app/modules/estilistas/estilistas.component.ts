import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SeriviciosService } from 'src/app/services/serivicios.service';
@Component({
  selector: 'app-estilistas',
  templateUrl: './estilistas.component.html',
  styleUrls: ['./estilistas.component.css']
})
export class EstilistasComponent implements OnInit {
estilistas: any;
  constructor(private ES: SeriviciosService, private router: Router) { }

  ngOnInit(): void {
    this.obtenerEstilistas();
  }

  obtenerEstilistas(){
    this.ES.getEstilistas().subscribe((data: any)=>{
      console.log(data);
      this.estilistas = data;
    });
  }

  detalles(id:number){
    console.log(id);
    this.router.navigate(['/estilista-Info',id]);
  }
  editar(id:number){
    console.log(id);
    this.router.navigate(['/update',id]);
  }

  eliminar(id:number){
    console.log(id);
    this.ES.deleteEstilista(id).subscribe((data: any)=>{
      console.log(data);
      this.obtenerEstilistas();
    })
    
  }

}
