import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SeriviciosService } from 'src/app/services/serivicios.service';

@Component({
  selector: 'app-catalogo-servicios',
  templateUrl: './catalogo-servicios.component.html',
  styleUrls: ['./catalogo-servicios.component.css']
})
export class CatalogoServiciosComponent implements OnInit {
servicios: any;
  constructor(private ES: SeriviciosService, private router: Router) { }

  ngOnInit(): void {
    this.obtenerServicios();
  }

  obtenerServicios(){
    this.ES.getServicios().subscribe((data: any)=>{
      console.log(data);
      this.servicios = data;
    });
  }

  detalles(id:number){
    console.log(id);
    this.router.navigate(['/card-detalle-servicio',id]);
  }

}
