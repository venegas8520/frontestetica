import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EstilistaInfoComponent } from './estilista-info.component';

describe('EstilistaInfoComponent', () => {
  let component: EstilistaInfoComponent;
  let fixture: ComponentFixture<EstilistaInfoComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EstilistaInfoComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(EstilistaInfoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
