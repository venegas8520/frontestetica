import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { SeriviciosService } from 'src/app/services/serivicios.service';
@Component({
  selector: 'app-estilista-info',
  templateUrl: './estilista-info.component.html',
  styleUrls: ['./estilista-info.component.css']
})
export class EstilistaInfoComponent implements OnInit {

  estilista = {
    id: "",
    nombre: "",
    apellido: "",
    edad: "",
    direccion: "",
    telefono: "",
    correo: "",
    foto: "",
    especialidad: ""
  }

  constructor(private ES: SeriviciosService, private AR: ActivatedRoute) { }
  id: any


  ngOnInit(): void {
    this.id= this.AR.snapshot.params['id'];
    this.obtenerEstilista();
  }

  obtenerEstilista(){
    this.ES.getEstilista(this.id).subscribe((data: any)=>{
      console.log(data);
      this.estilista = data;
    });
  }

  

}
