import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CitasComponent } from './usuarios/citas/citas.component';
import { CatalogoServiciosComponent } from './modules/catalogo-servicios/catalogo-servicios.component';
import { CatalogoProductosComponent } from './modules/catalogo-productos/catalogo-productos.component';
import { AgendarCitaComponent } from './modules/agendar-cita/agendar-cita.component';
import { NavbarUsuarioNormalComponent } from './shared/components/navbar-usuario-normal/navbar-usuario-normal.component';
import { CardDetalleServiciosComponent } from './shared/components/card-detalle-servicios/card-detalle-servicios.component';
import { CardProductoComponent } from './shared/components/card-producto/card-producto.component';
import { CardDetalleProductoComponent } from './shared/components/card-detalle-producto/card-detalle-producto.component';
import { ModalAddUsuarioComponent } from './shared/components/modal-add-usuario/modal-add-usuario.component';
import { ModalSeleccionarPagoComponent } from './shared/components/modal-seleccionar-pago/modal-seleccionar-pago.component';
import { FormularioIniciarSesionComponent } from './shared/components/formulario-iniciar-sesion/formulario-iniciar-sesion.component';
import { RegistroUsuarioComponent } from './shared/components/registro-usuario/registro-usuario.component';
import { PipesAppPipe } from './pipes/pipes-app.pipe';
import { HttpClientModule } from '@angular/common/http';
import { LayoutModule } from './layout.module';
import { ContactoComponent } from './modules/contacto/contacto.component';
import { EstilistasComponent } from './modules/estilistas/estilistas.component';
import { EstilistaInfoComponent } from './modules/estilista-info/estilista-info.component';
import { FooterComponent } from './shared/components/footer/footer.component';
import { CardEstilistaComponent } from './shared/components/card-estilista/card-estilista.component';
import { DashboardAdminComponent } from './estetica/dashboard-admin/dashboard-admin.component';
import { CitasPendientesComponent } from './estetica/citas-pendientes/citas-pendientes.component';
import { ProductosComponent } from './estetica/productos/productos.component';
import { UsuariosComponent } from './estetica/usuarios/usuarios.component';
import { PerfilComponent } from './usuarios/perfil/perfil.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { FormularioComponent } from './modules/formulario/formulario.component';
import { FormularioServicioComponent } from './modules/formulario-servicio/formulario-servicio.component';
import { VerServiciosComponent } from './modules/ver-servicios/ver-servicios.component';
import { EstilistasAdminComponent } from './estetica/estilistas-admin/estilistas-admin.component';
import { FormsServicioComponent } from './estetica/forms-servicio/forms-servicio.component';


@NgModule({
  declarations: [
    AppComponent,
    CitasComponent,
    CatalogoServiciosComponent,
    CatalogoProductosComponent,
    AgendarCitaComponent,
    CardDetalleServiciosComponent,
    CardProductoComponent,
    CardDetalleProductoComponent,
    ModalAddUsuarioComponent,
    ModalSeleccionarPagoComponent,
    FormularioIniciarSesionComponent,
    RegistroUsuarioComponent,
    PipesAppPipe,
    ContactoComponent,
    EstilistasComponent,
    EstilistaInfoComponent,
    FooterComponent,
    CardEstilistaComponent,
    DashboardAdminComponent,
    CitasPendientesComponent,
    ProductosComponent,
    UsuariosComponent,
    PerfilComponent,
    FormularioComponent,
    FormularioServicioComponent,
    VerServiciosComponent,
    EstilistasAdminComponent,
    FormsServicioComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    LayoutModule,
    HttpClientModule, 
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [],
  bootstrap: [AppComponent],
})
export class AppModule {}
