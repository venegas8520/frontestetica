import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { SeriviciosService } from 'src/app/services/serivicios.service';
@Component({
  selector: 'app-estilistas-admin',
  templateUrl: './estilistas-admin.component.html',
  styleUrls: ['./estilistas-admin.component.css']
})
export class EstilistasAdminComponent implements OnInit {
  estilistas: any;

  constructor(private ES: SeriviciosService, private router: Router) { }

  ngOnInit(): void {
    this.obtenerEstilistas();

  }

  obtenerEstilistas(){
    this.ES.getEstilistas().subscribe((data: any)=>{
      console.log(data);
      this.estilistas = data;
    });
  }

  detalles(id:number){
    console.log(id);
    this.router.navigate(['/estilista-Info',id]);
  }
  editar(id:number){
    console.log(id);
    this.router.navigate(['/update',id]);
  }

  eliminar(id:number){
    console.log(id);
    this.ES.deleteEstilista(id).subscribe((data: any)=>{
      console.log(data);
      this.obtenerEstilistas();
    })
    
  }

}
