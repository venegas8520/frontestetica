import { ComponentFixture, TestBed } from '@angular/core/testing';

import { EstilistasAdminComponent } from './estilistas-admin.component';

describe('EstilistasAdminComponent', () => {
  let component: EstilistasAdminComponent;
  let fixture: ComponentFixture<EstilistasAdminComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ EstilistasAdminComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(EstilistasAdminComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
