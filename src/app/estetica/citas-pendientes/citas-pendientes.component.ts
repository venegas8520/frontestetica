import { Component, OnInit } from '@angular/core';
import { SeriviciosService } from '../../services/serivicios.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-citas-pendientes',
  templateUrl: './citas-pendientes.component.html',
  styleUrls: ['./citas-pendientes.component.css']
})
export class CitasPendientesComponent implements OnInit {

  estado: any;
  citas: any;
  user: any;
  id: any;
  constructor(private ES: SeriviciosService, private router: Router) { 
    this.estado = true;
  }

  ngOnInit(): void {
    this.obtenercitas();
    this.obtenerUser(this.id);
  }
  obtenerUser(id: any) {
    this.ES.getUser(id).subscribe((data: any)=>{
      console.log(data);
      this.user = data;
    });
}


  obtenercitas(){
    this.ES.getCitas().subscribe((data: any)=>{
      console.log(data);
      this.citas = data;
    });
  }

  deletes(id: any) {
    console.log(id);
    this.ES.deleteCita(id).
    subscribe( newp => {
      console.log(newp);
    
        this.router.navigate(['/perfil-registrado']);
     
    });
  }

}
