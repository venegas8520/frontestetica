import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FormsServicioComponent } from './forms-servicio.component';

describe('FormsServicioComponent', () => {
  let component: FormsServicioComponent;
  let fixture: ComponentFixture<FormsServicioComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FormsServicioComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(FormsServicioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
