import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { SeriviciosService } from 'src/app/services/serivicios.service';

@Component({
  selector: 'app-forms-servicio',
  templateUrl: './forms-servicio.component.html',
  styleUrls: ['./forms-servicio.component.css']
})
export class FormsServicioComponent implements OnInit {

  servicio = {
    id: "",
    nombre: "",
    tipo: "",
    descripcion: "",
    costo: "",
    foto: "",
  }


  constructor(private ES: SeriviciosService, private router: Router, private AR: ActivatedRoute) { }
  id:any;

  ngOnInit(): void {
    this.id= this.AR.snapshot.params['id'];
    if(this.id){
      this.ES.getServicio(this.id).subscribe((data: any)=>{
        console.log(data);
        this.servicio = data;
      });
    }
  }

  save(form: NgForm){
    if(this.id){
      console.log(form.value);
      this.ES.updateServicio(form.value, this.id).subscribe((data:any)=>{
        console.log(data);
        this.router.navigate(['productos-admin']);
      });
    }else{
      console.log(form.value);
    this.ES.insertServicio(form.value).subscribe((data: any) => {
      if(data){
        console.log(data);
        this.router.navigate(['/productos-admin']);
      }
    });
    }
  }

}
